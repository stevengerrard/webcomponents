export function format(first: string, middle: string, last: string): string {
  return (first || '') + (middle ? ` ${middle}` : '') + (last ? ` ${last}` : '');
}

/**
 * @description
 * Función que formatea los número de manera abreviada
 * @param number Contiene un number
 * @returns Un string
 */
export function convertToMillion(number: number): string {

  if (isNaN(number)) return null;
  if (number === null) return null;
  if (number === 0) return null;
  let abs = Math.abs(number);
  const rounder = Math.pow(10, 1);
  const isNegative = number < 0;
  let key = '';

  const powers = [
    { key: 'Q', value: Math.pow(10, 15) },
    { key: 'T', value: Math.pow(10, 12) },
    { key: 'B', value: Math.pow(10, 9) },
    { key: 'M', value: Math.pow(10, 6) },
    { key: 'K', value: 1000 }
  ];

  for (let i = 0; i < powers.length; i++) {
    let reduced = abs / powers[ i ].value;
    reduced = Math.round(reduced * rounder) / rounder;
    if (reduced >= 1) {
      abs = reduced;
      key = powers[ i ].key;
      break;
    }
  }

  return (isNegative ? '-' : '') + abs + key;
}

/**
 * @description
 * Función que regresa el valor de una propiedad de un objeto
 * @param prop Contiene un objeto
 * @returns Un string
 */
export function getValueProperty(prop: any): string {

  if (prop) {
    const languages = [];
    const keys = Object.keys(prop);

    keys.forEach((propKey: any) => {

      if (typeof prop[ propKey ] === 'object') {
        languages.push(prop[ propKey ].name);
      } else {
        languages.push(prop[ propKey ]);
      }
    });

    return languages.join(', ');
  }

  return '';
}