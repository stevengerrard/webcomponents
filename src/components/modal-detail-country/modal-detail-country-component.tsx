import { Component, Event, EventEmitter, h, Prop, State } from '@stencil/core';
import { convertToMillion, getValueProperty } from '../../utils/utils';

@Component({
  tag: 'modal-detail-country',
  shadow: true,
  styleUrl: './modal-detail-country-component.css'
})
export class ModalDetailCountryComponent {

  @Prop() country = null;

  @State() showStar: boolean;

  @Event() closeModal: EventEmitter;
  @Event() switchFavorite: EventEmitter;

  componentWillLoad() {
    this.showStar = this.country?.favorite;
  }

  /**
   * @description
   * Función que cierra el modal
   */
  private onCloseModal(): void {
    this.closeModal.emit();
  }

  /**
   * @description
   * Función que emite la información del country
   * para ser agregado o quitado de favorito
   */
  private onSwitchFavorite(): void {
    this.country.favorite = this.country.favorite ? false : true;
    this.showStar = this.country.favorite;
    this.switchFavorite.emit(this.country);
  }

  render() {

    return (

      <div>
        <div id="myModal" class="modal">
          <div class="modal-content">

            <span class="close" onClick={() => this.onCloseModal()}>&times;</span>

            <h2>{this.country.name.common.toUpperCase()}

              {
                this.showStar ?
                  <span class="pointer" onClick={() => this.onSwitchFavorite()}>⭐</span>
                  :
                  <span class="pointer" onClick={() => this.onSwitchFavorite()}>✩</span>
              }
            </h2>

            <p>Region: {this.country.region}</p>
            <p>Population: {convertToMillion(this.country.population)}</p>
            <p>Capital: {this.country.capitalName}</p>
            <p>Currency: {getValueProperty(this.country.currencies)}</p>
            <p>Language: {getValueProperty(this.country.languages)}</p>
            <p>Border Countries: {this.country?.bordersFullName?.toString()?.split(',')?.join(', ') || 'No Borders'}</p>
            <p>Flag:</p>
            <p class="flag">{this.country.flag}</p>
          </div>
        </div>
      </div>
    );
  }
}
