# modal-detail-country



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type  | Default |
| --------- | --------- | ----------- | ----- | ------- |
| `country` | `country` |             | `any` | `null`  |


## Events

| Event            | Description | Type               |
| ---------------- | ----------- | ------------------ |
| `closeModal`     |             | `CustomEvent<any>` |
| `switchFavorite` |             | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
