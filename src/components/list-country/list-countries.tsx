import { Component, Event, EventEmitter, h, Prop } from "@stencil/core";

@Component({
  tag: 'list-countries',
  shadow: true,
  styleUrl: './list-countries.css'
})
export class ListCountries {

  @Prop() countriesList: any[];
  @Prop() showStar: boolean;

  @Event() sendCountry: EventEmitter;

  private onSenCountry(country: any): any {
    this.sendCountry.emit(country);
  }

  render() {
    return (
      <div>
        <ul>
          <li id="header-ul">
            {this.countriesList[ 0 ].region.toUpperCase()}
          </li>
          {
            this.countriesList.map((country: any) => {
              return (

                <li class="li pointer" onClick={() => this.onSenCountry(country)}>

                  <span>{country.flag}</span>

                  {country.name.common}

                  {
                    this.showStar ? <span>⭐</span> : ''
                  }

                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
}