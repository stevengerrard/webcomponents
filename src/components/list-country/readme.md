# list-countries



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute   | Description | Type      | Default     |
| --------------- | ----------- | ----------- | --------- | ----------- |
| `countriesList` | --          |             | `any[]`   | `undefined` |
| `showStar`      | `show-star` |             | `boolean` | `undefined` |


## Events

| Event         | Description | Type               |
| ------------- | ----------- | ------------------ |
| `sendCountry` |             | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
