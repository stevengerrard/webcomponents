import { Config } from '@stencil/core';
// import { angularOutputTarget, ValueAccessorConfig } from '@stencil/angular-output-target';

// const angularValueAccessorBindings: ValueAccessorConfig[] = [
//   {
//     elementSelectors: [ 'modal-detail-country' ],
//     event: '',
//     targetAttr: 'country',
//     type: 'boolean'
//   }
// ];


export const config: Config = {
  namespace: 'webcomponents',
  outputTargets: [
    // angularOutputTarget({
    //   componentCorePackage: 'component-library',
    //   directivesProxyFile: '../component-library-angular/src/directives/proxies.ts',
    //   valueAccessorConfigs: angularValueAccessorBindings,
    // }),
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements-bundle',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
    },
  ],
};
